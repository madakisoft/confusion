$("#carouselButton").click(function(){
    if ($("#carouselButton").children("span").hasClass('fa-pause')) {
        $("#mycarousel").carousel('pause');
        $("#carouselButton").children("span").removeClass('fa-pause');
        $("#carouselButton").children("span").addClass('fa-play');
    }
    else if ($("#carouselButton").children("span").hasClass('fa-play')){
        $("#mycarousel").carousel('cycle');
        $("#carouselButton").children("span").removeClass('fa-play');
        $("#carouselButton").children("span").addClass('fa-pause');                    
    }
});

$(function () {
    $("#btnShowLogin").click(function () {
        

        $("#LoginPopup .modal-title");
        $("#LoginPopup .modal-body");
        $("#LoginPopup").modal("show");
    });

    $("#btnCloseLogin").click(function () {
        $("#LoginPopup").modal("hide");
    });
});


$(function () {
    $("#btnShowReserveTable").click(function () {
        

        $("#ReserveTable .modal-title");
        $("#ReserveTable .modal-body");
        $("#ReserveTable").modal("show");
    });

    $("#btnCloseReserveTable").click(function () {
        $("#ReserveTable").modal("hide");
    });
});